#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <LittleFS.h>
#include <ArduinoJson.h>
#include <DHT.h>
#include <DHT_U.h>
#include <string.h>



// To disable DEBUG mode, comment out the #define line below
// #ifndef DEBUG
// #define DEBUG
// #endif

DHT_Unified dht(D4, DHT11);


/************************* Adafruit.io Setup *********************************/

const char* fingerprint PROGMEM = "59 3C 48 0A B1 8B 39 4E 0D 58 50 47 9A 13 55 60 CC A0 1D AF";

char mqttUsername[64];
char mqttKey[64];
char mqttHost[64];

/************ Global State (you don't need to change this!) ******************/

WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, mqttHost, 8883, mqttUsername, mqttKey);

/****************************** Feeds ***************************************/
char humidityFeedURI[128];
char temperatureFeedURI[128];
Adafruit_MQTT_Publish temperatureFeed = Adafruit_MQTT_Publish(&mqtt, temperatureFeedURI);
Adafruit_MQTT_Publish humidityFeed = Adafruit_MQTT_Publish(&mqtt, humidityFeedURI);

void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    // basically die and wait for WDT to reset me
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

void setup() {
  // USB Debugger setup
  Serial.begin(9600);

  #ifdef DEBUG
  Serial.println("[DEBUG] - Initializing LittleFS");
  #endif
  // Enable FileSystem
  if (!LittleFS.begin()){
    Serial.print("Can't mount filesystem; Exitting");
    // basically die and wait for WDT to reset me
    while (1);
  }
  
  
  StaticJsonDocument<512> doc;

  #ifdef DEBUG
  Serial.println("[DEBUG] - Grabbing file");
  #endif
  if(LittleFS.exists("/mqtt.json")){
    File file = LittleFS.open("/mqtt.json", "r");
    DeserializationError error = deserializeJson(doc, file);
    if (error) {
      Serial.println(F("Failed to read file, using default configuration"));
      while(1);
    }
      #ifdef DEBUG
      Serial.println("[DEBUG] - Deserializing Json");
      #endif
      strcpy(mqttUsername, doc["username"]);
      strcpy(mqttKey, doc["key"]);
      strcpy(mqttHost, doc["server"]);
      #ifdef DEBUG
      Serial.println("[DEBUG] - #### MQTT Config Dump ####");
      Serial.println(mqttUsername);
      Serial.println(mqttKey);
      Serial.println(mqttHost);
      #endif

    file.close();
  }
 
  #ifdef DEBUG
  Serial.println("[DEBUG] - Loading feed URIs");
  #endif
  strcat(temperatureFeedURI, mqttUsername);
  strcat(temperatureFeedURI, "/feeds/ambient-temperature");
  
  
  strcat(humidityFeedURI, mqttUsername);
  strcat(humidityFeedURI, "/feeds/ambient-relative-humidity");

  #ifdef DEBUG
  Serial.println("[DEBUG] - Starting DHT");
  #endif
  // Initialize the DHT Sensor
  dht.begin();
  
  #ifdef DEBUG
  Serial.println("[DEBUG] - Starting Wifi");
  #endif
  // Connect to Wifi
  WiFiManager wifimanager;

  #ifdef DEBUG
  Serial.println("[DEBUG] - Connect Wifi");
  #endif
  wifimanager.autoConnect();
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi Connected!");

  #ifdef DEBUG
  Serial.println("[DEBUG] - Prep SSL Fingerprint for MQTT client");
  #endif
  client.setFingerprint(fingerprint);
  
  // Print debug messages if in DEBUG mode
  #ifdef DEBUG
  Serial.println("==== AGS - Ambiant Sensor ====");
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println(F("------------------------------------"));
  #endif
}

void loop() {
  delay(5000);
  MQTT_connect();

  sensors_event_t event;
  dht.temperature().getEvent(&event);
  int temperature = event.temperature;
  if(temperatureFeed.publish(temperature)){
    #ifdef DEBUG
    Serial.println("Ok! - Temperature");
    #endif
  } else {
    #ifdef DEBUG
    Serial.println("Failed to send temperature reading");
    #endif
  }


  dht.humidity().getEvent(&event);
  int humidity = event.relative_humidity;
  if(humidityFeed.publish(humidity)){
    #ifdef DEBUG
    Serial.println("Ok! - Humidity");
    #endif
  } else {
    #ifdef DEBUG
    Serial.println("Failed to send relative humidity reading");
    #endif
  }


  #ifdef DEBUG
  String debugString1("Temp: ");
  String debugString2("C / Humidity: ");
  String debugString3("%");
  Serial.println(debugString1 + temperature + debugString2 + humidity + debugString3);
  #endif

}